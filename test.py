import os
import sys
import unittest
#from models import db, Book
from create_db import db, Book

class DBTestCases(unittest.TestCase):
    def test_source_insert_1(self):
        s = Book(title = 'C++')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Book).filter_by(title='C++').one()
        self.assertEqual(str(r.title), 'C++')

        db.session.query(Book).filter_by(title='C++').delete()
        db.session.commit()

    def test_source_insert_2(self):
        s = Book(title = 'C++')
        db.session.add(s)
        db.session.commit()

        r = db.session.query(Book).filter_by(title='C++').one()
        self.assertEqual(r.isbn, None)

        db.session.query(Book).filter_by(title = 'C++').delete()
        db.session.commit()

    def test_source_select(self):
        s = Book(isbn='20', title = 'C++')
        q = Book(isbn='19', title = 'pypython')
        db.session.add_all([s, q])
        db.session.commit()

        r = db.session.query(Book).filter_by(isbn = '20').all()
        self.assertEqual(len(r), 1)

        db.session.query(Book).filter_by(isbn = '20').delete()
        db.session.query(Book).filter_by(isbn = '19').delete()
        db.session.commit()


if __name__ == '__main__':
    unittest.main()
