from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING", 'postgres://postgres:asd123@localhost:5432/bookdb')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # suppresses a warning message
app.config['WTF_CSRF_ENABLED'] = False # suppress Flask-wtf cross-site registry forgery protection
db = SQLAlchemy(app)

class Book(db.Model):
	__tablename__ = 'book'

	title = db.Column(db.String(80), primary_key = True)
	google_id = db.Column(db.String(20), nullable = True)
	isbn = db.Column(db.String(20), nullable = True)
	publication_date = db.Column(db.String(12), nullable = True)
	subtitle = db.Column(db.String(200), nullable = True)
	image_url = db.Column(db.String(80), nullable = True)
	description = db.Column(db.String(3500), nullable = True)
	author = db.Column(db.String(100), nullable = True)
	publisher = db.Column(db.String(100), nullable = True)
	genre = db.Column(db.String(30), nullable = True)
	died = db.Column(db.String(100), nullable = True)

	# under publisher

	founded = db.Column(db.String(100), nullable = True)
	location = db.Column(db.String(100), nullable = True)
	pub_wiki_url = db.Column(db.String(100), nullable = True)
	pub_description = db.Column(db.String(3000), nullable = True)
	pub_parent_company = db.Column(db.String(100), nullable = True)
	pub_website = db.Column(db.String(100), nullable = True)
	pub_num_books = db.Column(db.String(4), nullable = True)

	# under author

	born = db.Column(db.String(100), nullable = True)
	nationality = db.Column(db.String(100), nullable = True)
	auth_desc = db.Column(db.String(1000), nullable = True)
	auth_wiki_url = db.Column(db.String(200), nullable = True)
	alma_mater = db.Column(db.String(100), nullable = True)
	auth_image_url = db.Column(db.String(600), nullable = True)

db.drop_all()
db.create_all()
