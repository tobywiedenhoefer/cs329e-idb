import subprocess
from forms import SearchForm
from flask import Flask, url_for, render_template, request, redirect, g
from create_db import app, db, Book, create_books
from sqlalchemy import or_

# helper function
# returns a list of UNIQUE publishers
def uniq_pub(books):
    pub_db = []
    for i in books:
        unique = True
        for j in pub_db:
            if i.publisher == j.publisher:
                unique = False
        if unique:
            pub_db.append(i)
    return pub_db

# helper function
# returns books of same publisher
def same_pub(unique, books):
    same = []
    for i in books:
        if i.publisher == unique.publisher:
            same.append(i)
    return same


# home page
@app.route('/')
def index():
    return render_template('home.html')


# page for books
@app.route('/books/')
def books():
    books = db.session.query(Book).all()
    search = SearchForm(request.form)
    if request.method == 'POST':
        return search_results(search)

    return render_template('books.html', books = books)

# abstract book
@app.route('/books/<bookname>')
def generic_book(bookname):
    this_book = db.session.query(Book).filter_by(title=bookname).first()
    books = db.session.query(Book).all()

    return render_template('generic_book.html', book=this_book, books=books)


# page for Authors
@app.route('/authors/')
def authors():
    authdata = db.session.query(Book.author, Book.born,
                                Book.nationality, Book.alma_mater, Book.died)
    # turning into a set to kill dups
    authors = set(a for a in authdata)

    return render_template('authors.html', authors=authors)

# abstract author
@app.route('/authors/<authname>')
def generic_author(authname):
    matching_books = db.session.query(Book).filter_by(author=authname)
    return render_template('generic_author.html', books=matching_books)


# page for publishers
@app.route('/publishers/')
def publishers():
    books = uniq_pub(db.session.query(Book).all())
    return render_template('publishers.html', books = books)

# abstract publisher
@app.route('/publishers/<pubname>')
def generic_publisher(pubname):
    books = db.session.query(Book).all()
    matching_book = db.session.query(Book).filter_by(publisher=pubname).first()
    pub_books = same_pub(matching_book, books)
    return render_template('generic_publisher.html', book=matching_book, pub_books=pub_books)


# about us page
@app.route('/about/', methods=['GET', 'POST'])
def about():
    return render_template('about.html')


# search handler
@app.route('/search', methods=['POST'])
def search():
    if not g.search_form.validate_on_submit():
        return redirect(url_for('index'))

    return redirect(url_for('search_results', qry=g.search_form.search.data))

# search results/filtering (SUPER basic, match qry as substring in important cols)
@app.route('/search/<qry>')
def search_results(qry):
    originalqry = qry
    # make raw query ready to place in SQL 'LIKE' statement
    qry = qry.strip()
    qry = "%{}%".format(qry)
    if qry:
        results = db.session.query(Book).filter(or_(
            Book.title.ilike(qry), Book.author.ilike(qry),
            Book.isbn.ilike(qry), Book.publisher.ilike(qry))
        ).limit(50).all()
    else:
        results = []

    return render_template('results.html', qry=originalqry, results=results)


# before POST request handling
@app.before_request
def before_request():
    # store search form in Flask's global 'g' variable
    # makes available to all templates
    g.search_form = SearchForm()


# unit tests
@app.route('/test/')
def test():
    p = subprocess.Popen(["coverage", "run", "--branch", "test.py"],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            stdin=subprocess.PIPE)
    out, err = p.communicate()
    output=err+out
    output = output.decode("utf-8") #convert from byte type to string type

    return render_template('test.html', output = "<br/>".join(output.split("\n")))


if __name__ == "__main__":
    app.run()
