import json
from models import app, db, Book

def load_json(filename):
	with open(filename) as file:
		jsn = json.load(file)
		file.close()
	return jsn

def create_books():
	book = load_json('books.json')
	for i in book:
		p = i['publishers'][0]
		a = i['authors'][0]
		for x in range(11):
			try:
				if x == 0:
					title = i['title']
				elif x == 1:
					google_id = i['google_id']
				elif x == 2:
					isbn = i['isbn']
				elif x == 3:
					publication_date = i['publication_date']
				elif x == 4:
					subtitle = i['subtitle']
				elif x == 5:
					image_url = i['image_url']
				elif x == 6:
					description = i['description']
				elif x == 7:
					author = a['name']
				elif x == 8:
					publisher = p['name']
				elif x == 9:
					genre = i['genre']
				elif x == 10:
					died = a['died']
			except:
				if x == 0:
					title = ''
				elif x == 1:
					google_id = ''
				elif x == 2:
					isbn = ''
				elif x == 3:
					publication_date = ''
				elif x == 4:
					subtitle = ''
				elif x == 5:
					image_url = ''
				elif x == 6:
					description = ''
				elif x == 7:
					author = ''
				elif x == 8:
					publisher = ''
				elif x == 9:
					genre = ''
				elif x == 10:
					died = ''

		# exclusive for publisher data
		for x in range(7):
			try:
				if x == 0:
					founded = p['founded']
				elif x == 1:
					location = p['location']
				elif x == 2:
					pub_wiki_url = p['wikipedia_url']
				elif x == 3:
					pub_description = p['description']
				elif x == 4:
					pub_parent_company = p['parent company']
				elif x == 5:
					pub_website = p['website']
				elif x ==6:
					pub_num_books = p['book_num']
			except:
				if x == 0:
					founded = ''
				elif x == 1:
					location = ''
				elif x == 2:
					pub_wiki_url = ''
				elif x == 3:
					pub_description = ''
				elif x == 4:
					pub_parent_company = ''
				elif x == 5:
					pub_website = ''
				elif x == 6:
					pub_num_books = ''

		# exclusive for author data
		for x in range(6):
			try:
				if x == 0:
					born = a['born']
				elif x == 1:
					nationality = a['nationality']
				elif x == 2:
					auth_desc = a['description']
				elif x == 3:
					auth_wiki_url = a['wikipedia_url']
				elif x == 4:
					alma_mater = a['alma_mater']
				elif x == 5:
					auth_image_url = a['image_url']
			except:

				if x == 0:
					born = ''
				elif x == 1:
					nationality = ''
				elif x == 2:
					auth_desc = ''
				elif x == 3:
					auth_wiki_url = ''
				elif x == 4:
					alma_mater = ''
				elif x == 5:
					auth_image_url = ''

		newBook = Book(title = title, google_id = google_id, isbn=isbn, publication_date = publication_date, subtitle = subtitle, image_url = image_url, description = description, author = author, publisher = publisher, founded = founded, location = location, pub_wiki_url = pub_wiki_url, pub_description = pub_description, pub_parent_company = pub_parent_company, pub_website = pub_website, born = born, nationality = nationality, auth_desc = auth_desc, auth_wiki_url = auth_wiki_url, alma_mater = alma_mater, auth_image_url = auth_image_url, genre = genre, pub_num_books = pub_num_books, died = died)
		db.session.add(newBook)
		db.session.commit()

create_books()
